#!/bin/bash

## DD_Me.sh


#------------------------------------------------------------------------------
# Created by ELIAS WALKER
#------------------------------------------------------------------------------
# started 16/Jan/2019 undergoing development to date.
#------------------------------------------------------------------------------
# BOW is Copyright/Trademark pending 2018 by ELIAS WALKER
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; under version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
#
# Or visit
# https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
###############################################################################
####################### Acknowledgements ######################################
###############################################################################
#
# I wrote this script from scratch, it was brought to life by  
# an overwhelming number of requests by the arrowlinux group.
#
# For contributions, code, ideas, and other help,
# I give my deepest thanks to: knowledgebase101, erw1030, 
# leon.p (From EzeeTalk), and the arrowlinux group.
###############################################################################
#
#       If you find this script useful
#
#       help keep this project afloat. donate to
#
#       paypal.me/eliwal
#
###############################################################################
##################### Run this script (as root) ###############################
###############################################################################
#
#------------------------------------------------------------------------------
# This part Checks to see of it is ran as root. If not, it will close.
# It also checks if Zenity is installed. If not, it will install it.
TODAY="$(date)"

if [[ $USER = "root"  ]]
 then
        clear
      if [ -f /usr/bin/dialog ]
           then clear
                clear
                 echo " Dialog is already installed"
                 echo ""
           else pacman -Sy --noconfirm dialog
                clear
                clear
      fi
  echo " ROOT check passed "
  echo ""
  echo " =================================================="
  echo " ========== $TODAY =========="
  else
         dialog --no-lines --msgbox "ROOT check FAILED!\n\nIn a terminal run this with sudo.\n\nlike this     sudo ./bow.sh\nOr this       sudo bash bow.sh" 10 40 #background-color: red
         dialog --no-lines --msgbox "BOW will now close ! ! " 6 30
         clear
         clear
       echo " ROOT check FAILED (must be ran with sudo) like this "
       echo ""
       echo " sudo ./bow.sh"
       echo ""
       echo " =================================================== "
        exit "${?}"
fi


DWS1='DD Me'
title2='Write an ISO image to USB'

DD_a_USB="Write An ISO To USB"
SDDUSB="Start DD Process"
EX1T='EXIT DD Me'
EXT2='Back to Home Menu'
EXT3='Just Exit This App'

DDME='DD a USB'
title=' Z-DDme toolbox'
EXT1='EXIT Z-DDme'
BACK_HOME='Back to Home Menu'     


# home, update
HOME_HEIGHT=15
HOME_WIDTH=33
HOME_CHOICE_HEIGHT=8

DD_HEIGHT=13
DD_WIDTH=40
DD_CHOICE_HEIGHT=6

Q_HEIGHT=15
Q_WIDTH=27
Q_CHOICE_HEIGHT=9

EXIT_HEIGHT=26
EXIT_WIDTH=40
EXIT_CHOICE_HEIGHT=24
while true ; do

MENU="Choose an option:"

OPTIONS=(1 "$DD_a_USB"
         2 "$EXT3")

CHOICE=$(dialog --clear \
                --ok-label "continue" \
                --no-cancel \
                --no-lines \
                --backtitle "$DWS1" \
                --title "$UPO1" \
                --menu "$MENU" \
                $HOME_HEIGHT $HOME_WIDTH $HOME_CHOICE_HEIGHT \
                "${OPTIONS[@]}" \
                2>&1 >/dev/tty)

clear
case $CHOICE in
###############################################################################
########################### Update Management #################################
###############################################################################

        1)
                 while [[ "$?" = "0" ]] ; do
                 OPTIONS=(1 "$SDDUSB" 
                          2 "$EXT2"
                          3 "$EX1T")

                 CHOICE=$(dialog --clear \
                                 --ok-label "Continue" \
                                 --cancel-label "Back" \
                                 --no-lines \
                                 --backtitle "$DWS1" \
                                 --title "$UPO1" \
                                 --menu "$MENU" \
                                 $DD_HEIGHT $DD_WIDTH $DD_CHOICE_HEIGHT \
                                 "${OPTIONS[@]}" \
                                 2>&1 >/dev/tty)
                 case $CHOICE in
                         1) 

                                  while [[ "$?" = "0" ]] ; do
                                  OPTIONS=(1 "$SDDUSB" 
                                           2 "$EXT2"
                                           3 "$EX1T")

                         CHOICE=$(dialog --clear \
                                      --ok-label "Continue" \
                                      --cancel-label "Back" \
                                      --no-lines \
                                      --backtitle "$DWS1" \
                                      --title "$UPO1" \
                                      --menu "$MENU" \
                                      $DD_HEIGHT $DD_WIDTH $DD_CHOICE_HEIGHT \
                                      "${OPTIONS[@]}" \
                                      2>&1 >/dev/tty)
                         case $CHOICE in
                                         1) 
                                            clear ; clear ; echo "" ; echo "choose file locaion test Passed" ; echo "" 
                                            echo ""
                                            echo "Press a key to continue"
                                            read -rsn1
                                            echo "Key pressed" ; sleep 1
                                            ;;

                                         2)
                                            clear && clear && exit 1
                                            ;;

                                         3)
                                            clear && clear && break 3
                                            ;;
                         esac
                         done

                 esac
                 done    


            ;;
         

         2) clear && clear && break 2
            ;;
esac
done